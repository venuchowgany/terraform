terraform {
  backend "s3" {
      bucket = "tftestbuck"
      key = "ntier/ntier_state"
      region = "us-east-1"
      dynamodb_table = "forterraform"

  }
}